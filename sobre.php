<?php include "menu.php"; ?>

<div class="topo-height">
	<div class="width-max">
		<div class="secao-sobre">
			<div class="sobre-info">
				<div class="bloco-sobre">
					<div class="imagem-circle">
						<img class="" style="width: 100%" src="assets/images/about/clayton.jpg">
					</div>
				</div>
				<div class="sobre-pessoais">
					<div class="bloco-sobre">
						<span class="titulo-3">Age</span>
						<span class="texto">25 years old</span>
					</div>
					<div class="bloco-sobre">
						<span class="titulo-3">Living in</span>
						<span class="texto">Florianópolis</span>
					</div>
					<div class="bloco-sobre">
						<span class="titulo-3">Experience</span>
						<span class="texto">+6 years</span>
					</div>
				</div>
			</div>
			
			<div class="sobre-conteudo">
				<span class="titulo-3">About me</span>
				<span class="texto">
					Since my childhood i'm passionate about technology and over time i became more and more involved with the area and began to deepen in computation in high school in the year 2009 with the integrated technical course of Web Designer. The course lasted approximately 2 years and provided the opportunity to acquire a greater knowledge and the certainty of being on the right track, seeking simple solutions to complex problems and prioritizing the needs of the user.<br /><br />
					During high school I did some work as a freelancer, but I actually entered the job market as soon as I started college in the course of Systems Analysis and Development that began in 2012. The choice of course could not have been better, because it was exactly what i wanted to add in my life, because as i already had done the technical course in web design, i wanted to understand how things worked in development. Understanding the part of creation and how to develop has made me a more complete professional, and helps me design designs knowing the limitations of development and delivering workable solutions.<br /><br />
					At the same time as I started the college I also began to work in Poliedro, company of the area of education. I joined as a trainee and was hired in a few months as a Web Designer, i took Photoshop and Illustrator courses at Adobe Solutions and stayed in the company until the end of 2013, shortly before starting my journey at Titaniumfix where i worked with a great team worried with the user in a collaborative and interactive process. <br /><br />
					My focus on UX design is explained by the frustration of how often people are prisoners to poorly designed products that make their lives more difficult. My goal is to design products and experiences that are meaningful and impactful, and I am especially interested in using new technologies to improve people's lives in innovative ways.

				</span>
				<span class="titulo-3">
						Things I like to do
				</span>
				<div class="sobre-liked">
					<div class="liked-bloco">
						<img class="" src="assets/images/about/liked-acampamento.svg">
						<span class="texto">Camping</span>
					</div>
					<div class="liked-bloco">
						<img class="" src="assets/images/about/liked-assistir.svg">
						<span class="texto">Watch</span>
					</div>
					<div class="liked-bloco">
						<img class="" src="assets/images/about/liked-fast-food.svg">
						<span class="texto">Fast food</span>
					</div>
					<div class="liked-bloco">
						<img class="" src="assets/images/about/liked-games.svg">
						<span class="texto">Games</span>
					</div>
					<div class="liked-bloco">
						<img class="" src="assets/images/about/liked-futebol.svg">
						<span class="texto">Soccer</span>
					</div>
					<div class="liked-bloco">
						<img class="" src="assets/images/about/liked-livros.svg">
						<span class="texto">Read</span>
					</div>
					<div class="liked-bloco">
						<img class="" src="assets/images/about/liked-musica.svg">
						<span class="texto">Music</span>
					</div>
					<div class="liked-bloco">
						<img class="" src="assets/images/about/liked-redes.svg">
						<span class="texto">Social media</span>
					</div>
					<div class="liked-bloco">
						<img class="" style="width: 100%" src="assets/images/about/liked-viagem.svg">
						<span class="texto">Travel</span>
					</div>
					<div class="liked-bloco">
						<img class="" style="width: 100%" src="assets/images/about/liked-bicicleta.svg">
						<span class="texto">Bike</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include "rodape.php"; ?>


