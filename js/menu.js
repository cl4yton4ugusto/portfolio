'use strict';

console.clear();

var app = function () {
	var body = undefined;
	var menu = undefined;
	var menuItems = undefined;

	var init = function init() {
		body = document.querySelector('body');
		menu = document.querySelector('.menu-icon');
		menuItems = document.querySelectorAll('.nav__list-item');

		applyListeners();
	};

	var applyListeners = function applyListeners() {
		menu.addEventListener('click', function () {
			return toggleClass(body, 'nav-active');
		});
	};

	var toggleClass = function toggleClass(element, stringClass) {
		if (element.classList.contains(stringClass)) element.classList.remove(stringClass);else element.classList.add(stringClass);
	};

	init();
}();

/* FUNDO MENU APARECE COM SCROLL 

$(window).scroll(function() {
	var menuDesktop = $(".menu0");
	var menuMobile = $(".menu-icon-fixed");

	    var scroll = $(window).scrollTop();
	    if (scroll >= 20) {
	        menuDesktop.addClass("menu-scrolled");
	        menuMobile.addClass("menu-scrolled");
	        $(".menu-projeto .menu0 .menu0__list a").css("color", "#000");
	        $(".menu-projeto .menu-icon__line").css("background-color", "#000");
	    } else {
	        menuDesktop.removeClass("menu-scrolled");
	        menuMobile.removeClass("menu-scrolled");
	        $(".menu-projeto .menu0 .menu0__list a").css("color", "#fff");
	        $(".menu-projeto .menu-icon__line").css("background-color", "#fff");
	    }
});