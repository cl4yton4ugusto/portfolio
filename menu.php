<!DOCTYPE html>
<html >
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121411246-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-121411246-1');
		</script>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" href="assets/favicon.ico" type="image/x-icon">
	<title>Clayton Augusto</title>

	<!-- CSS -->
	<link rel="stylesheet" href="css/custom.css">
	<link rel="stylesheet" href="css/menu.css">
</head>

<body>

<!-- MENU ATIVO -->
<?php
function active($currect_page){
  $url_array =  explode('/', $_SERVER['REQUEST_URI']) ;
  $url = end($url_array);  
  if($currect_page == $url){
      echo 'active'; //class name in css 
  } 
}
?>	

<!-- MENU MOBILE -->
<div class="menu-icon">
	<span class="menu-icon__line menu-icon__line-left"></span>
	<span class="menu-icon__line"></span>
	<span class="menu-icon__line menu-icon__line-right"></span>
</div>
<a href="index"><img class="menu-logo-fixed" src="assets/logo.gif"/></a>
<div class="menu-icon-fixed"></div>
<div class="nav">
	<div class="nav__content">
		<ul class="nav__list">
			<a class="<?php active('index');?>" href="index"><li class="nav__list-item">Home</li></a>
			<a class="<?php active('sobre');?>" href="sobre"><li class="nav__list-item">About</li></a>
			<a class="<?php active('trajetoria');?>" href="trajetoria"><li class="nav__list-item">Trajetory</li></a>
			<a class="<?php active('processo');?>" href="processo"><li class="nav__list-item">Process</li></a>
			<a class="<?php active('projetos');?>" href="projetos"><li class="nav__list-item">Projects</li></a>
			<!-- <a href="contato"><li class="nav__list-item">Contact</li></a> -->
		</ul>
	</div>
</div>

<!-- MENU DESKTOP-->
<div class="menu0">
	<ul class="menu0__list">
		<a class="<?php active('index');?>" href="index"><li class="menu0__list-item">Home</li></a>
		<a class="<?php active('sobre');?>" href="sobre"><li class="menu0__list-item">About</li></a>
		<a class="<?php active('trajetoria');?>" href="trajetoria"><li class="menu0__list-item">Trajetory</li></a>
		<a class="<?php active('processo');?>" href="processo"><li class="menu0__list-item">Process</li></a>
		<a class="<?php active('projetos');?>" href="projetos"><li class="menu0__list-item">Projects</li></a>
		<!-- <a href="contato"><li class="menu0__list-item">Contact</li></a> -->
	</ul>	
</div>