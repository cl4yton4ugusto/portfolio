<?php include "menu.php"; ?>

<!-- HEADER-->
<div class="width-max site-header">
	<p class="texto">Hello,</p>
	<h1 class="titulo-1"><strong>I'm Clayton Augusto,<br /> and I design <span id="typed" class="textChanging"></span></strong></h1>
	<div id="typed-strings">
		<p>experiences.</p>
		<p>products.</p>
		<p>dreams.</p>
	</div>
	<p class="texto">Whatever you want to call it. My pleasure is in developing new ways to make you happy. This might mean partnering up and creating a new product or service, or it might just mean improving what’s already there — saving time and money.</p>
</div>

<!-- PORTOLIO-->
<?php include "grid-projetos.php"; ?>

<!-- FUNDAMENTOS-->
<div class="fundo-cinza">
	<div class="width-max">
        <div class="titulo-2">Desivgn for me is about making the bridge between users and digital artifacts by:</div>

		<div class="fundamentos-bloco">
            <div class="bloco">
                <div class="fundamentos-imagem1"></div>
                <div class="titulo-4">Communicating info</div>
			</div>
    		<div class="bloco">
    			<div class="fundamentos-imagem2"></div>
                <div class="titulo-4">Engaging attention</div>
    		</div>
    		<div class="bloco">
    			<div class="fundamentos-imagem3"></div>
                <div class="titulo-4">Facilitating action</div>
    		</div>
        </div>

        <div class="texto">I focus on designing holistically and delightfully, using iterative processes, careful thought, and the help of type, color, and motion.</div>
	</div>
</div>


<div class='icon-scroll'></div>


<?php include "rodape.php"; ?>