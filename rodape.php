<div class="width-full contato-rodape">
<!-- CONTATO -->
  <div class="contato-bloco">
      <div class="titulo-2">There is a project you’d like to discuss? I'd love to hear you.</div>
      <a target="_blank" href="https://api.whatsapp.com/send?phone=5548991899042&text=Olá,%20estou%20aguardando%20sua%20conversa."><div class="botao">Contact me</div></a>
  </div> 


<!-- RODAPE -->
  <div class="copy-social">
      <div class="texto copy">© 2018 Design and development by <strong>Clayton Augusto</strong></div>
      <div class="social-icones">
      	<a target="_blank" href="https://www.linkedin.com/in/clayton-augusto-ferraz/"><div class="imagem-social-1"></div></a>
      	<a target="_blank" href=""><div class="imagem-social-2"></div></a>
      	<a target="_blank" href="https://www.facebook.com/cl4yton4ugusto"><div class="imagem-social-3"></div></a>
      	<a target="_blank" href="https://www.instagram.com/claytonaugusto_/"><div class="imagem-social-4"></div></a>
      </div>
  </div>
</div>

<div class="languages"><span>EN</span><a href="">PT</a></div>

<!-- JQUERY --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- MENU --> 
<script src="js/menu.js"></script>

<!-- TYPED -->
<script src="js/typed.js"></script>
<script type="text/javascript">
	var typed = new Typed("#typed", {
    stringsElement: '#typed-strings',
    typeSpeed: 90,
    backSpeed: 50,
    startDelay: 2000,
    loop: true,    
  });
</script>

<!-- SUMIR COM SCROLL -->
<script type="text/javascript">
  $(window).scroll(function() {
    if ($(this).scrollTop()>0)
     {
        $('.icon-scroll').fadeOut();
     }
    else
     {
      $('.icon-scroll').fadeIn();
    }
  });
</script>


</body>
</html>
