<?php include "menu.php"; ?>

<div class="topo-height">
  <div class="width-max">
  	<section id="cd-timeline">

      <div class="cd-timeline-block">
        <div class="cd-timeline-img cd-work">
          <img src="assets/images/trajetory/work.svg">
        </div> <!-- cd-timeline-img -->
        <div class="cd-timeline-content">
          <h2 class="titulo-3">UX Designer</h2>
          <p class="texto">
            <b>Bonitour Viagens e turismo</b><br><br>
            Leading the design team, I conduct brainstorming sessions with clients, understand and analyze their requirements, personas, user stories. Using research methods i discover user pain points, user needs (user interview), competitor analysis, MVP analysis while observing the users in real environment. Storyboard, user flow, sitemap, scenario and use case, sketches, Lo-fi wireframe, design strategy, style guides, clickable prototype and then conduct usability tests and implement user feedback, monitoring, a/b testing and heuristic analysis.
          </p>
          <span class="cd-date texto">May, 2017 to Actual</span>
        </div> <!-- cd-timeline-content -->
      </div> <!-- cd-timeline-block -->

      <div class="cd-timeline-block">
        <div class="cd-timeline-img cd-work">
          <img src="assets/images/trajetory/work.svg">
        </div> <!-- cd-timeline-img -->
        <div class="cd-timeline-content">
          <h2 class="titulo-3">Freelancer</h2>
          <p class="texto">
            <b>MOB.ag</b><br><br>
            I have worked for numerous clients such as: Hai Toyota, Hit Honda, Schumman, Floripa Harley-Davidson making responsive development of website and landing page, email marketing in HTML5, creation of graphic elements such as image processing and vectors, banners animation for social media, video editing, social network management, website design and blogging with SEO practices, among other activities.
          </p>
          <span class="cd-date texto">Feb, 2017 to May, 2017</span>
        </div> <!-- cd-timeline-content -->
      </div> <!-- cd-timeline-block -->

  		<div class="cd-timeline-block">
  			<div class="cd-timeline-img cd-academic">
  				<img src="assets/images/trajetory/academic.svg">
  			</div> <!-- cd-timeline-img -->
  			<div class="cd-timeline-content">
  				<h2 class="titulo-3">Analysis and systems development</h2>
          <p class="texto">
            <b>ETEP (São José dos Campos)</b><br /><br />
            College degree</p>
  				<span class="cd-date texto">Feb, 2012 to Feb, 2016</span>
  			</div> <!-- cd-timeline-content -->
  		</div> <!-- cd-timeline-block -->

  		<div class="cd-timeline-block">
  			<div class="cd-timeline-img cd-work">
  				<img src="assets/images/trajetory/work.svg">
  			</div> <!-- cd-timeline-img -->
  			<div class="cd-timeline-content">
  				<h2 class="titulo-3">UX/Web designer</h2>
  				<p class="texto">
            <b>Titaniumfix</b><br /><br />
            Responsible for understanding user needs, competitive analysis, creating the website interface, responsive development of the website and web system, testing with users, structuring the site with SEO practices, creating mobile application layout, preparing web content as banners and pictures. Creation of arts for various media, layout of catalogs and materials for advertising, manipulation and texturing of 3D images, email marketing in HTML5, editing and creation of videos, management of social networks, etc.
          </p>
  				<span class="cd-date texto">Nov, 2014 to Feb, 2017</span>
  			</div> <!-- cd-timeline-content -->
  		</div> <!-- cd-timeline-block -->

      <div class="cd-timeline-block">
        <div class="cd-timeline-img cd-academic">
          <img src="assets/images/trajetory/academic.svg">
        </div> <!-- cd-timeline-img -->
        <div class="cd-timeline-content">
          <h2 class="titulo-3">Illustrator e Photoshop</h2>
          <p class="texto"><b>Adobe Solutions</b></p>
          <span class="cd-date texto">Apr, 2013</span>
        </div> <!-- cd-timeline-content -->
      </div> <!-- cd-timeline-block -->

      <div class="cd-timeline-block">
        <div class="cd-timeline-img cd-work">
          <img src="assets/images/trajetory/work.svg">
        </div> <!-- cd-timeline-img -->
        <div class="cd-timeline-content">
          <h2 class="titulo-3">Web designer</h2>
          <p class="texto">
            <b>Poliedro</b><br><br>
            In the design team i participated in brainstorming meetings, understanding stakeholder needs, personas, creating the interface of the website with educational content and development, layout and programming of educational games for students, usability analysis, banners, diagramming, among other things.
          </p>
          <span class="cd-date texto">Aug, 2012 to Nov, 2013</span>
        </div> <!-- cd-timeline-content -->
      </div> <!-- cd-timeline-block -->

      <div class="cd-timeline-block">
        <div class="cd-timeline-img cd-academic">
          <img src="assets/images/trajetory/academic.svg">
        </div> <!-- cd-timeline-img -->
        <div class="cd-timeline-content">
          <h2 class="titulo-3">Web Design Technician</h2>
          <p class="texto">
            <b>Colégio Adventista de São José dos Campos</b><br /><br />
            Technical license
          </p>
          <span class="cd-date texto">Feb, 2009 to Nov, 2010</span>
        </div> <!-- cd-timeline-content -->
      </div> <!-- cd-timeline-block -->

  	</section> <!-- cd-timeline -->
  </div>
</div>

<?php include "rodape.php"; ?>


