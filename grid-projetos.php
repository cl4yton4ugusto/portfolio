<!-- PORTOLIO-->
<div class="width-full">
	<div class="portfolio-conteudo">
		<div class="grid">
            <figure class="effect-lily">
                <img src="assets/images/projects/project-chat/job-home.jpg" alt="img12"/>
                <figcaption class="projeto-chat">
                    <div>
                        <h2><span>Chat</span> App</h2>
                        <p>User Interface</p>
                    </div>
                    <a href="projetos_chat">View more</a>
                </figcaption>           
            </figure>
        </div>
		<div class="grid">
			<figure class="effect-lily">
				<img src="assets/images/projects/project-foodrest/job-home.jpg" alt="img12"/>
				<figcaption class="projeto-foodrest">
					<div>
						<h2><span>FoodRest</span> App</h2>
						<p>User Interface</p>
					</div>
					<a href="projetos_foodrest">View more</a>
				</figcaption>			
			</figure>
		</div>
		<div class="grid">
			<figure class="effect-lily">
				<img src="assets/images/projects/project-binamik/job-home.jpg" alt="img12"/>
				<figcaption class="projeto-binamik">
					<div>
						<h2><span>Binamik</span> App</h2>
						<p>User Experience, User Interface</p>
					</div>
					<a href="projetos_binamik">View more</a>
				</figcaption>			
			</figure>
		</div>
        <div class="grid projeto-bonitour">
			<figure class="effect-lily">
				<img src="assets/images/projects/project-bonitour/job-home.jpg" alt="img12"/>
				<figcaption>
					<div>
						<h2>Bonitour <span>WEBSITE</span></h2>
						<p>User Experience, User Interface</p>
					</div>
					<a href="projetos_roteiro-facil">View more</a>
				</figcaption>			
			</figure>
		</div>
	</div>
</div>