<?php include "menu.php"; ?>

<!-- HEADER-->
	<div class="projeto-header banner-binamik"></div>

	<!-- CONTEUDO-->
	<div class="width-max projeto-roteirofacil">
		<div class="secao-intro">
			<div class="intro-info">
				<div class="bloco-info">
					<span class="titulo-3">Company</span>
					<span class="texto">Fictício</span>
				</div>
				<div class="bloco-info">
					<span class="titulo-3">Year</span>
					<span class="texto">2017</span>
				</div>
				<div class="bloco-info">
					<span class="titulo-3">Platform</span>
					<span class="texto">Website</span>
				</div>
				<div class="bloco-info">
					<span class="titulo-3">Tools</span>
					<span class="texto">Illustrator</span>
					<span class="texto">Photoshop</span>
					<span class="texto">Adobe XD</span>
					<span class="texto">Sublime 3</span>
				</div>
			</div>
			<div class="intro-conteudo">
				<h1 class="titulo-1">Binamik</h1>

				<span class="tipo-tag">UI</span>
				<span class="tipo-tag">Interaction</span>
				<span class="tipo-tag">Graphic design</span>

				<span class="texto">
					Roteiro Fácil is a tool for those who do not imagine a life without traveling, who likes adventure in ecological destinations.
				</span>
				<!-- TIPO -->
				<span class="titulo-3">
					Goals
				</span>
				<span class="texto">
					The city of Bonito in Mato Grosso do Sul has an average visitor growth of 12% per year. Considered an ecotourism hub in the country, Bonito has grown on a large scale and attracted the attention of tourists.<br /><br />

					The main feature of the tool is to reduce the complexity of creating travel itineraries for Bonito, simplifying ticket purchase processes, booking tours, and lodging.
				</span>
				<!-- TIPO -->
				<span class="titulo-3">
					Challenges
				</span>
				<span class="texto">
					Bonito is a destination known for its natural beauties, but what not everyone knows is that when planning the trip there are some items that directly interfere on how you will enjoy the trip. Some examples of Bonito's challenges:<br /><br />

					<b>The tours have a limit of people:</b> the advance in the reservation is obligatory because the attractions have a maximum number of people to be there every day. For this reason, in the most crowded times, the most attractive attractions are sold out soon.<br /><br />

					<b>You can only buy tickets at the agency:</b> to control the limit of people per trip, the City Hall has a centralized system of vouchers, which can only be issued at the agencies. Therefore, it is not possible to go straight into the attraction to buy.<br /><br />

					<b>The time of year will directly influence what will be used in Bonito:</b> from December to March is a rainy season. Therefore, the forest becomes greener. From May to September, in the dry season, the rivers become more crystalline and full of fish.<br /><br />

					<b>Most of the attractions are far from the city, so transportation must be thought of previously:</b> despite being far away, Bonito does not have public transportation. That's why even taxis and motorbike taxis are not afraid to charge. Then tell them you need to hire a car or rent a car.<br /><br />

					A tourism professional, an agent, usually takes about 6 months to master all the complexities. It is an arduous time because there are many variables that make up the assembly of a complete script. And if to train a person takes so long, imagine presenting all the complexity of creating the script for a lay user in minutes.
				</span>
				<!-- TIPO -->
				<span class="titulo-3">
					Solution
				</span>
				<span class="texto">
					Seeking to lessen the impact of the complexities in the creation of the script, the Roteiro Fácil will offer everything the user needs to know before packing and venturing into Bonito in a simple way. The site will provide all the options of attractions, lodging and transport. It will also provide information about each of them, such as what to bring in each attractiveness, commitment time, schedules and so on.<br /><br />

					The tool not only seeks to sell products separately, but to assemble the whole schedule, to take the complexity of the user and finally to provide all the experience that he seeks in Bonito.

				</span>
			</div>
		</div>
	</div>

	<div class="imagem-full">
		<img class="full-inside" src="assets/images/projects/project-1/bonito.jpg">
	</div>

	<div class="width-max">
		<div class="secao-persona">
			<span class="titulo-3">User stories</span>
			<div class="persona-bloco">
				<img style="width: 100%" class="" src="assets/images/projects/project-1/user_story.png">
			</div>
		</div>
	</div>

	<div class="fundo-cinza">
		<div class="width-max">
			<div class="secao-persona">
				<span class="titulo-3">Persona</span>
				<div class="persona-bloco">
					<div class="persona-bloco-menor"></div>
					<div class="persona-bloco-maior">
						<span class="titulo-3">Bio</span>
						<span  class="texto">
							Ainda não tem filhos, mas já se encontra num momento muito estável na vida. Gosta de viajar com as amigas ou um parceiro.
						</span>

						<span class="titulo-3">Necessidades</span>
						<span  class="texto">
							Descansar da rotina e da cidade grande.<br />
							Conhecer novos destinos e novas culturas.<br />
							Busca conforto e segurança para suas viagens.
						</span>

						<span class="titulo-3">Características</span>
						<span  class="texto">
							Não tem noção da complexidade de montar um roteiro, está sempre na correria e sente-se insegura com poucas informações
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	
<div class='icon-scroll'></div>

<?php include "rodape.php"; ?>
