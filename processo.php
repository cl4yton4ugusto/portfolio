<?php include "menu.php"; ?>

<div class="topo-height">
	<div class="width-max">
		<div class="processo-intro">
			<span class="texto">Design is no longer an artistic practice and creativity is not just an artist’s emotional expression. Designers are the solution to solving problems that enable people, businesses, and technology to impact how the world works. Through personal experience, experiments, and research, I’ve evolved from being an artist into a scientific designer. Designers who are systematic, methodic, and process driven innovate more strategically.
			</span>
			<img src="assets/images/process/grafico1.png" />
			<span class="texto">My process is about gaining an understanding of the big picture, identifying goals and pain-points, and taking a User-Centered approach in designing usable solutions. The methods and techniques I use within each phase of my process may vary depending on the design challenges.
			</span>
		</div>
	</div>

	<div class="fundo-cinza">
		<div class="width-max">
	        <div class="titulo-2">My design process has the following items:</div>

			<div class="processo-item">
	            <div class="item">
	                <div><div class="processo-imagem1"></div></div>
	                <div class="item-textos">
	                	<span class="titulo-4">Understand</span>
	                	<span class="texto">Conduct brainstorming sessions with stakeholders and understand and analyze of yours requirements, goals, challenges, personas, user stories.</span>
	                </div>
				</div>
	    		<div class="item">
	    			<div><div class="processo-imagem2"></div></div>
	    			<div class="item-textos">
		                <span class="titulo-4">Research</span>
		                <span class="texto">Using user research methods to discover user pain points, user needs (user interview, card sorting, survey), competitor analysis, MVP analysis while observing the users in real environment.</span>
		            </div>
	    		</div>
	    		<div class="item">
	    			<div><div class="processo-imagem3"></div></div>
	    			<div class="item-textos">
		                <span class="titulo-4">Ideate</span>
		                <span class="texto">Synthesize research and brainstorm possible solutions, storyboard, user flow, sitemap, scenario and use case, sketches.</span>
		            </div>
	    		</div>
	    		<div class="item">
	    			<div><div class="processo-imagem4"></div></div>
	    			<div class="item-textos">
	                	<span class="titulo-4">Design</span>
	                	<span class="texto">Design strategy, style guides, lo-fi wireframe, refine user interface and create clickable prototype.</span>
	                </div>
	    		</div>
	    		<div class="item">
	    			<div><div class="processo-imagem5"></div></div>
	    			<div class="item-textos">
	                	<span class="titulo-4">User Test</span>
	                	<span class="texto">Conduct usability tests and implement user feedback, monitoring, a/b testing and heuristic analysis.</span>
	                </div>
	    		</div>
	        </div>
		</div>
	</div>

	<div class="width-max">
		<div class="processo-tools">
			<div class="tools-icones">
				<img class="" src="assets/images/process/tools/tools-xd.svg">
				<img class="" src="assets/images/process/tools/tools-invision.svg">
				<img class="" src="assets/images/process/tools/tools-sketch.svg">
				<img class="" src="assets/images/process/tools/tools-code.svg">
				<img class="" src="assets/images/process/tools/tools-git.svg">
				<img class="" src="assets/images/process/tools/tools-illustrator.svg">
			</div>
			<div class="tools-descricao">
				<div>
					<span class="titulo-4">Creation</span>
					<span class="texto">
						Illustrator<br />
						Photoshop<br />
						Gliphy<br />
						Premiere<br />
						After Effects
					</span>
				</div>
				<div>
					<span class="titulo-4">Prototyping</span>
					<span class="texto">
						Invision<br />
						Adobe XD<br />
						Sketch<br />
						Figma<br />
						Marvel
					</span>
				</div>
				<div>
					<span class="titulo-4">Development</span>
					<span class="texto">
						HTML<br />
						CSS<br />
						Javascript<br />
						GitHub<br />
					</span>
				</div>
				<div>
					<span class="titulo-4">Metrcis</span>
					<span class="texto">
						Analytics<br />
						Hotjar<br />
						User testing
					</span>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include "rodape.php"; ?>


